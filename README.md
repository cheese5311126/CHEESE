# CHEESE OFFICIAL DOCUMENTATION REPOSITORY

This repository contains a compilation of the various CHEESE software catalogs, classified into five principal groups: codes, mini-apps, audits, simulation cases, and workflows.

## REPOSITORY STRUCTURE

This repository follows the following structure:

```

CHEESE
  |- CODES                        
  |    |- FALL3D                  
  |    |    |- FALL3D-CPU            
  |    |    |   |- metadata.json      
  |    |    |   |- README.md            
  |    |    |   |- .gitmodules
  |    |    |   |- FALL3D-CPU
  |    |    |       |- src                 
  |    |    |- FALL3D-GPU            
  |    |    |   |- metadata.json      
  |    |    |   |- README.md            
  |    |    |   |- .gitmodules
  |    |    |   |- FALL3D-GPU
  |    |    |       |- src
  |    |- HYSEA
  |    |    |- TSUNAMI-HYSEA
  |            ...
  |- MINI-APP
  |    |- Mini-FALL3D                  
  |    |    |- Mini-FALL3D-CPU            
  |    |    |   |- metadata.json      
  |    |    |   |- README.md            
  |    |    |   |- .gitmodules
  |    |    |   |- Mini-FALL3D-CPU
  |    |    |       |- src                 
  |    |    |- Mini-FALL3D-GPU            
  |    |    |   |- metadata.json      
  |    |    |   |- README.md            
  |    |    |   |- .gitmodules
  |    |    |   |- Mini-FALL3D-GPU
  |    |    |       |- src
  |    |- HYSEA
  |    |    |- LAVAFLOW-HYSEA
  |            ...
  |- SIMULATION CASES
  |    |- SC1.1
  |    |    |- metadata.json
  |    |    |- README.md
  |    |- SC1.2
  |         ...
  |- AUDITS
  |    |- FALL3D
  |    |    |- M1
  |    |    |    |- ?
  |    ...
  |- WORKFLOWS
  |    |- PD1
  |    |    |- ?
  |    |- PD2
  |    ...
  

  ```
## How to fill in the metadata and README files for each repository?

The README file must contain a short description of the code, mini-app, audit, workflow, or simulation case. The minimal and mandatory elements are title, description, authorship, license, affiliation and contacts.

The metadata file is a JSON schema based on the RDA DMP (Research Data Alliance Data Management Plan) Common Standard, which is designed to create a flexible and comprehensive metadata application profile for data management plans.
In this repository, you can find three examples of this schema: a minimal schema, a more extended schema with the principal fields, and a comprehensive example with all the sections you can use. Furthermore, you can take a look at the FALL3D repository to see the minimal structure we require.

You can find more information related to this schema [here](https://github.com/RDA-DMP-Common/RDA-DMP-Common-Standard)

## Adding more repositories and git submodules

If any app or code is missing, you can ask for permission to add a new repository, always respecting the original structure.
Furthermore, if you want to add a new git submodule, you can follow these instructions:

#### 1) Clone the current repository

In order to add or update an existing Git submodule, you shall fetch the latest version of the `main` branch of the target repository. For instance, `git clone` it if you don't have a local copy (`git pull` if you already cloned it before): 

```bash
$ git clone https://gitlab.com/cheese5311126/codes/fall3d/new_code_name.git
```
<ins>Note</ins>: do not use `--recurse-submodules` option as it will ask for authentication for all the not publicly accessible submodules.

#### 2) Add a new step in the workflow

**_This action shall only be done if the step has not yet been added to the list of submodules_**. 

The repository containing the code of the workflow step must be tracked as a Git submodule. So, every time a new step is required to be added to a given workflow, the repository must be added with:

```bash
$ git submodule add <url> <path>
```

where:
- `<url>` is the URL to the upstream repository. (https://gitlab.com/fall3d/new_code_name/)
- `<path>` is the relative location of the step within the workflow. (new_code_name/src)

This command adds a new definition of the step in the `.gitmodules` file.

As mentioned earlier, there are two main approaches to maintain the code for a step in a workflow:

1. Use an individual Git repostory, e.g.:

```bash
$ git submodule add https://gitlab.com/fall3d/new_code_name new_code_name/src
```

<ins>Note</ins>:
- If the repository added as a Git submodule is private, the `git submodule add` command will prompt for credentials.


```bash
$ cat .gitmodules
[submodule "new_code_name/src"]
	path = new_code_name/src
	url = https://gitlab.com/fall3d/new_code_name 
```

**COMMIT & PUSH CHANGES**: at this point you must commit and push the changes done, otherwise they won't be tracked in the Workflow Registry. E.g.:

```bash
git commit -a -m "Registering new repository new_code_name"
git push <remote> <feature/branch>
```

where:
- `<remote>` is the remote name. The name `origin` is the one defined by default, but you could ensure that it points to the right repository by running `git remote -v`.
- `<feature/branch>` is the branch created in [section 2)](#2-add-the-required-changes).

<ins>Note</ins>: Write permission is required for the push action to work. Please contact maintainers to add your GitLab account as a contributor.



